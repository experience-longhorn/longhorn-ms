const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const AssetsPlugin = require('assets-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = {
    entry: {
        main: "./src/ts/main.ts"
    },
    devtool: "source-map",
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: "ts-loader",
                exclude: /node_modules/
            },
            {
                test: /\.scss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: "css-loader",
                        options: {
                            sourceMap: true
                        }
                    },
                    {
                        loader: "sass-loader",
                        options: {
                            sourceMap: true
                        }
                    }
                ]
            }
        ]
    },
    resolve: {
        extensions: [ ".tsx", ".ts", ".js" ]
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: "[contenthash]-[name].css",
            chunkFilename: "[id].css"
        }),
        new AssetsPlugin({
            filename: 'webpack_assets.json',
            path: path.join(__dirname, 'data'),
            prettyPrint: true,
            entrypoints: true
        }),
        new CleanWebpackPlugin({
            cleanOnceBeforeBuildPatterns: ["*"],
        })
    ],
    optimization: {
        splitChunks: {
            chunks: "all"
        }
    },
    output: {
        filename: "[contenthash]-[name].js",
        path: path.resolve(__dirname, "static/dist"),
        publicPath: "/dist/"
    }
}