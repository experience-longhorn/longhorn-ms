---
title: "The Experience Longhorn Project"
---

# What is Longhorn?

"Longhorn" was the codename for the operating system that was to be Windows XP's successor. Initially intended to be a minor release, the project grew in scope until it became the next major release of Windows instead.

This codename taken from the name of a bar situated between the mountains of Whistler (used as the codename of Windows XP) and Blackcomb (which was the codename for a post-Longhorn major release of Windows). Whistler and Blackcomb were part of the Whistler Blackcomb ski resort in British Columbia, which Microsoft employees visited regularly from just over the US-Canada border.

As ambitions grew and the scope of the Longhorn project expanded, problems continued to mount up, increasing in both number and severity. Eventually, in the summer of 2004, Microsoft took the decision to reset the project and begin again from the latest release codebase, which was used in Windows Server 2003 and in some 64-bit Windows XP releases. Three years after Microsoft originally intended to launch the successor to Windows XP, Microsoft released Longhorn as Windows Vista in January 2007.

## About Longhorn.ms
The goal of the Experience Longhorn project is to document the history, technical detail and functionality within Longhorn builds.

This website is currently maintained by Thomas Hounsell. You can submit changes via pull requests on our [GitLab repository](https://gitlab.com/experience-longhorn/longhorn-ms). Text content on the website is licenced under [CC-BY-SA-4.0](https://creativecommons.org/licenses/by-sa/4.0/), except where otherwise noted.