---
title: Install any WIM-based Longhorn build from USB
author: Thomas Hounsell
type: post
date: 2012-03-29T01:00:00+00:00
url: /boot-from-usb/
categories:
- Installer & Setup

---

One common problem faced by people installing Longhorn on modern hardware is the apparent inability to boot from USB drives. As optical media grows rarer, and more and more form factors ship without optical media drives, booting from a disc is not always an option. Fortunately, booting Longhorn through USB can be a surprisingly easy affair once you know what you’re doing.

There are two different ways to install Longhorn from USB, depending on which build you are using.

## Longhorn WinPEs & Early WIM Builds

This is the easier scenario, and applies to earlier builds where the WinPE existed outside of the WIM and custom-made WinPEs that have been extracted from the WIM of later builds, as is detailed in [this handy guide](https://uxunleaked.blogspot.com/2008/05/howto-longhorn-winpe-windows.html) by Grabberslasher.

1. Download and install [RMPrepUSB](https://rmprepusb.com/) – this is by far the simplest way to make your flash drive bootable without sacrificing flexibility.
2. Select your flash drive and set a label, or accept the default.
3. Select the XP/BartPE bootable (NTLDR) option.
4. You can choose any file system at this point, all should work for Longhorn, but FAT32 is widely regarded as best practice in this scenario.
5. I personally suggest you chose Boot as HDD (C: 2PTNS). Different choices at this stage work for different people, but this option works more reliably with modern hardware for me.
6. Copy the contents of the original ISO, or the WinPE to the flash drive.
7. Copy SETUPLDR.BIN, NTDETECT.COM and TXTSETUP.SIF from the BOOT folder to the root of the drive.
8. Rename BOOT to MININT and SETUPLDR.BIN to NTLDR.
9. You should now have a bootable flash drive.

## Later Builds

Later builds require a different method. They need to read the WIM to load the WinPE. These builds include all from 4038 onwards. Previously, the only solution was to use Grabberslasher’s guide to extract the Windows PE, and then install Longhorn using this extracted Windows PE, but I disliked this method and researched a simpler alternative that allows the WinPE to boot from inside the WIM.

You can now prepare a bootable USB flash drive for later builds by doing the following:

1. Follow the instructions for the earlier builds, up to and including the stage where you copy the files from the ISO onto the flash drive.
2. Download this ZIP containing a [WIM-Friendly NTLDR](wim-friendly-ntldr.zip)
3. Extract the ZIP and place the NTLDR onto the root of the flash drive.
4. Your flash drive should now be bootable.

You should bear in mind that simply because your flash drive is now bootable, does not mean that Longhorn will boot on your hardware. Longhorn has a number of incompatibilities with more modern hardware, some of which can lead to stop errors or boot failures.

_Taken from an article that originally appeared on the WIMPack website, written by Thomas Hounsell_