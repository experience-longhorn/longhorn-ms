---
title: Running in VMWare
author: Thomas Hounsell
type: post
date: 2020-09-01T00:00:00+00:00
url: /vmware-drivers/
categories:
  - Downloads
  - Installer & Setup

---

## Driver Packages
These packages are some extracted drivers from the VMWare Guest Additions. The tools themselves are not compatible with Longhorn, although the drivers themselves maintained compatibility for much longer.

* [VMware 6.5 driver package](/download/vmware-6-5-driver-package.zip)
* [VMware 6.5.2 driver package](/download/vmware-6-5-2-driver-package.zip)
* [VMware 7.0 driver package](/download/vmware-7-0-driver-package.zip)
* [Internet Archive Backup](https://archive.org/details/longhorn-vmware-driver-packages)

## VMWare Configuration Tweaks
In addition to the drivers above, you may wish to add the following lines to your virtual machine configuration file (*.vmx) to disable time sync:

{{< highlight ini >}}
tools.syncTime = "FALSE"
time.synchronize.continue = "FALSE"
time.synchronize.restore = "FALSE"
time.synchronize.resume.disk = "FALSE"
time.synchronize.shrink = "FALSE"
time.synchronize.tools.startup = "FALSE"
time.synchronize.tools.enable = "FALSE"
time.synchronize.resume.host = "FALSE"
{{< / highlight >}}

If you have a newer Ryzen CPU in your host machine, you may find that some of the earlier builds do not boot within VMWare. You can fix this by overriding the CPUID presented within the virtual machine to an older AMD CPU using these lines in your configuration file:

{{< highlight ini >}}
cpuid.1.eax = "0000:0000:0000:0000:0000:0110:0100:0010"
cpuid.brandString = "AMD Athlon(tm)"
{{< / highlight >}}