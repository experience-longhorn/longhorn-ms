---
title: Behind the wallpapers
author: Melcher
type: post
date: 2018-06-26T22:17:33+00:00
url: /behind-the-wallpapers/
featured_image: ws_Vista_grass_1600x1200.jpg
categories:
  - Development & History

---
Here's just a funny tidbit I found some time ago to keep you guys entertained. In [an interview on Channel9](https://web.archive.org/web/20140502023251/https://channel9.msdn.com/Shows/WM_IN/Jenny-Lam-Designing-Experiences-at-Microsoft "Jenny Lam - Designing Experiences at Microsoft"), Robert Scoble asks a designer on the Windows team, Jenny Lam some questions about Vista and how its coming up Channel9. During the interview the following comes up on the subject of wallpapers:

> **S:** So, when we're going to get Vista, what are we going to see that is yours?
> 
> **L:** I drive the desktop wallpaper and screen saver. I'm working on...
> 
> **S:** So, you're the one responsible for all the green grass I see all over campus?
> 
> **L:** Yeah. Well see, that's kind of a trick cause that was ... we want to save all our "thunder" for launch, so we wanted to come up with something that really showed of the glass really well. So when you run the glass over the blades you can actually see the refraction, and you can get a sense of dimension and depth. It's higher fidelity, but it wasn't another wink to anything in the future. It's, sort of, a placeholder - but then a nicer version. But, yeah... I'm responsible for grass. Even the bails of hay in alpha - I don't know if you remember that (...)

<video width="100%" preload="metadata" controls="controls" poster="https://archive.org/download/jenny-lam-designing-experiences-at-microsoft/__ia_thumb.jpg">
  <source type="video/mp4" src="https://archive.org/download/jenny-lam-designing-experiences-at-microsoft/jenny_lam_windows_vista_designer_2005.mp4" />
</video>

So know you know who but all those hay bails in the Longhorn alphas 😉 It's kinda interesting that this was all thought through so well; grass because of the glass. And it's very true: it does show the refraction very nicely.

That's it for this time. See ya.

{{< grid >}}
{{< figure src="Longhorn_Alpha_Bliss.jpg" link="Longhorn_Alpha_Bliss.jpg" target="_blank" alt="Hay bails wallpaper used in Milestone 3" >}}
{{< figure src="ws_Vista_grass_1600x1200.jpg" link="ws_Vista_grass_1600x1200.jpg" target="_blank" alt="Grass wallpaper used in Vista Beta 1 era builds">}}
{{< /grid >}}