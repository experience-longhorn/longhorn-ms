---
title: Fixed 4093 Releases
author: Melcher
type: post
date: 2016-04-20T18:08:50+00:00
url: /fixed-version-4093/
featured_image: 4093-fixed-1.png
categories:
  - Downloads

---

The original leak of Longhorn build 4093 from within Microsoft was broken and uninstallable. Prior to leaking the release to the public, the C0d3rz release group had attempted to repair the ISO and at least make it installable. While they loosely succeeded in this goal, the resulting product introduced a number of additional bugs and no effort had been expended to keep it as original as possible.

With this in mind, several attempts have been made over the years to reverse some of the damage done in the original leak and restore it to something closer to Microsoft's original intention.

## Original Longhorn.ms Release from Melcher and Lukáš

Build 4093 was originally leaked at 28 August 2006 as a heavily edited ISO.  As the original 4093 ISO wasn't bootable, the C0d3rz team made a bootable ISO using 4033's WinPE. They edited `startnet.cmd` to start 4093's setup from the usa\_4093\_x86fre.pro\_longhorn directory in the ISO.

Until today (1st May 2014) the C0d3rz ISO was to be used if one would like to install 4093. Not anymore, as Lukáš in collaboration with Melcher were able to put the original 4093 WinPE to work. We, from longhorn.ms, proudly present to you, the proper release of Longhorn Build 4093 with its own PE!

Install notes:
* Unzip
* Mount or burn image
* Install with this key: `TCP8W-T8PQJ-WWRRH-QH76C-99FBW`


### Gallery

{{< gallery 50 "4093-fixed-1.png" "4093-fixed-3.png" "4093-fixed-4.png" "4093-fixed-2.png" >}}

## Later revisions by Lukáš
Two later revisions have been made available by Lukáš, who wanted to improve further on his and Melcher's original release for longhorn.ms. These were released to the BetaWiki Discord.

The first was from 2019, and the second from 2020. The main aim of these releases was to bundle the WinPE back into the WIM, rather than use an extracted WIM stored losely in the disc image.

## Downloads

All versions discussed on this page can be [downloaded from Archive.org](https://archive.org/details/longhorn-4093-repaired-isos).