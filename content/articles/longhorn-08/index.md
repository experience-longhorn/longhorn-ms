---
title: Longhorn 08
author: Thomas Hounsell
type: post
date: 2021-05-15T01:00:00+00:00
url: /longhorn-08/
draft: true
categories:
- Projects

---

Longhorn 08 was a project that ran in 2007 to 2008, comprising mostly of people from the recent, aborted Longhorn Reloaded project. In order to circumvent the distribution issues that led to the demise of Longhorn Reloaded, the decision was made to distribute it as a patch, rather than as a complete ISO.

In many respects, Longhorn 08 owed more to the previous TrueLonghorn patch than it did to the Longhorn Reloaded project.

## Beta 1


## Beta 2
[Download Longhorn 08 Beta 2](longhorn-08-beta-2.zip)