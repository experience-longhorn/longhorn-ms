---
title: Creating a sidebar tile in build 3683
author: Thomas Hounsell
type: post
date: 2021-10-08T00:00:00+00:00
url: /creating-a-tile-3683/
featured_image: 3683-sample-tile.jpg
categories:
  - APIs

---

A couple of years ago, I put together a sample tile written in .NET 1.1 / Visual Studio 2003 for Longhorn build 3683. This sample tile simply reads the current OS Version from `Environment.OSVersion`.

{{< figure src="3683-sample-tile.jpg" alt="The sample tile running in Longhorn Build 3683" >}}

There are two methods that your class must implement if you inherit from the BaseTile abstract class.

The first is `HasView`:

{{< highlight cs >}}
public abstract bool HasView(ModuleViewTypes view);
{{< / highlight >}}

This is used by the Sidebar to determine which "views" your tile supports. In this sample, we only implement the `ModuleViewTypes.SideBar` view, which is the basic view of a tile in a sidebar. To indicate this, we return true only if the view argument is equal to `SideBar`.

In Longhorn build 3683, there are two other view types supported:
* `ModuleViewTypes.Flyout` - This is for the fly out when the tile on the sidebar is clicked on.
* `ModuleViewTypes.Customize` - This is for providing a settings / configuration page. This is accessed via a Customize context menu item.

The other method we must implement is `GetView`.

{{< highlight cs >}}
public abstract Element GetView(ModuleViewTypes view);
{{< / highlight >}}

This method returns the Avalon element containing the view indicated by the `view` argument. For unsupported types, it is best to just return `null`.

This can be done by loading XAML, though for simplicity in my sample, I have used code to build the tree manually. This is also how many of the included tiles are implemented - though this is possibly because they were compiled to CAML.

In my sample, I've also overriden the Initialize method. I call the base method to include the default setup provided by `BaseTile`, but it also allows me to access the `IModuleViewHost` interface as the tile loads:

{{< highlight cs >}}
public void Initialize(IModuleViewHost moduleViewHost) {}
{{< / highlight >}}

This provides a useful method that allows us to set the tile header data. In our case, I pass a title - "Longhorn Version", but leave the additional information and icon set to `null`.

{{< highlight cs >}}
void SetHeaderInfo(string title, string additionalInfo, ImageData icon);
{{< / highlight >}}

This is a relatively simple task, it simply involves creating a Class Library containing a class that implements the sidebar tile abstract class (`BaseTile`).

Adding the tile to Explorer is relatively simple.

{{< highlight reg >}}
Windows Registry Editor Version 5.00

[HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\StartBar\Modules\VersionTile]
"Assembly"="SampleTiles"
"Module"="SampleTiles.BuildVersionTile"
{{< / highlight >}}

The Assembly value specifies the assembly that contains the tile, and the Module value specifies the class within that module that implements the tile interface.

The Sidebar will look for .NET Assemblies matching the Assembly name in the following locations:
* `%CD%\<Assembly>.dll`
* `%WINDIR%\<Assembly>.dll`
* `%WINDIR%\<Assembly>.exe`
* `%WINDIR%\<Assembly>\<Assembly>.dll`
* `%WINDIR%\<Assembly>\<Assembly>.exe`

The current directory for a typical `explorer.exe` process is `%USERPROFILE%`, i.e. `C:\Documents and Settings\User`.

You can [download the sample tile source code](https://gitlab.com/experience-longhorn/sample-tiles-3683) from this GitLab repository.