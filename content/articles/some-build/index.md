---
title: Some build
author: Melcher
type: post
date: 2017-05-17T17:47:17+00:00
url: /some-build/
featured_image: 408x-2.png
categories:
  - Development & History

---
I'm on some sort of "write down all the things" spree, [documenting things](https://www.betaarchive.com/forum/viewtopic.php?f=62&t=33865 "BetaArchive - Longhorn for everyday use (back then)") I've found in the past, but never properly have written down here on longhorn.ms. Today I'd like to have a brief look at this unidentified build.

In the video ["Why I love XAML"](https://web.archive.org/web/20111202143426/https://channel9.msdn.com/Blogs/TheChannel9Team/Joe-Marini-Why-I-love-XAML "Channel 9 - Joe Marini - Why I love XAML") on Channel9, published 20 August 2004, Joe Marini shows of an Avalon application powered by data binding, all of which is contained in a single XAML file. During the video we see Joe using a Longhorn build.  The video was originally recorded at 27 July 2004 which places this identifies this build as a mid to late 408x-range build - assuming this build was installed recently as part of the selfhosting/dogfooding program.

<video width="100%" preload="metadata" controls="controls" poster="https://archive.org/download/joe-marini-why-i-love-xaml/__ia_thumb.jpg">
  <source type="video/mp4" src="https://archive.org/download/joe-marini-why-i-love-xaml/Joe-Marini-Why-I-love-XAML_mid.mp4" />
</video>

[Download MP4 on Internet Archive](https://archive.org/details/joe-marini-why-i-love-xaml)

The image below is the only overview shot of this build we get to see in this video.

{{< figure src="408xoverview.png" link="408xoverview.png" target="_blank" alt="Video frame showing overall screenshot of unknown Longhorn build" >}}

The explorer style does indeed suggest we're dealing with a late Longhorn build: no plus sign in front of the "Storage favourites' item and no folder image in the preview pane behind the "X item in this folder' text both of which were still present in build 4074. Moreover, the iconic tree wallpaper seen in post-4074 builds.

{{< figure src="408x-2.png" link="408x-2.png" target="_blank" alt="A close-up of Windows Explorer in the unknown Longhorn build" >}}

Note that the preview pane is colored purple for this folder which indicates it's a pictures and video folder. Usually, folders containing documents have their default greyish blue color.

The files in the folder and the XAML file that can be seen during the video were published by Joe Marini on his personal website as part of [a tutorial on data-binding](https://web.archive.org/web/20041011115109/http://www.joemarini.com:80/tutorials/tutorialpages/xamldataboundui.php "joemarini.com - Building Data-Bound Use Interfaces in XAML"). The tutorial states that the code is compatible with build 4074, otherwise known as the WinHEC Longhorn build.

{{< gallery third "DataBoundUI1.jpg" "DataBoundUI2.jpg" "DataBoundUI3.jpg" >}}