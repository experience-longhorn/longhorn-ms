---
title: My TV and Movies
author: Melcher
type: post
date: 2014-10-25T20:17:03+00:00
url: /my-tv-and-movies/
featured_image: mytv-guide.png
categories:
  - Shell & User Interface

---
The "My TV and Movies" _library_ becomes visible when using the Media Center Edition (_"Freestyle"_) variant of some Longhorn builds. The "My TV and Movies" library is a media application for recording and watching TV. The application is an Avalon container and is opened in Internet Explorer by the Avalon Shell Handler. The container itself includes a couple of dll and BAML files as well as a manifest.

{{< grid >}}
{{< figure src="mytv-3718.png" link="mytv-3718.png" target="_blank" alt="The My TV and Movies app container in Longhorn build 3718" >}}
{{< figure src="mytv-guide.png" link="mytv-guide.png" target="_blank" alt="The included (non-functional) TV Guide UI placeholder in the My TV and Movies application within Longhorn build 3718" >}}
{{< /grid >}}

In some builds the filesize is noticeably bigger because it includes placeholder images. There is a placeholder image for the guide, but you will recieve an exception when trying to view this page. If you do not have Media Center installed, the exception will be because it cannot initialise the EPG (Electronic Program Guide). Otherwise, the exception will be a CSS-related parsing error.

A placeholder for "Watch Live TV" is also included, but the page will just show up empty. In a later version of `MyTVapp` found in 4008 and 4015, the application will throw an exception.

{{< grid >}}
{{< figure src="mytv-3706.png" link="mytv-3706.png" target="_blank" alt="The included (non-functional) Recordings UI placeholder in the My TV and Movies application within Longhorn build 3706" >}}
{{< figure src="mytv-4008.png" link="mytv-4008.png" target="_blank" alt="An error is shown attempting to load the My TV and Movies application within Longhorn build 4008 and later" >}}
{{< /grid >}}

To open the application run `C:\Windows\System32\mytvapp.container` or convert Longhorn from Professional to Media Center Edition. This application is present in builds: 3706, 3713, 3718, 4001, 4008, 4015 and 4029.