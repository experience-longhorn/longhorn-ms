---
title: Branding APIs in Longhorn
author: Thomas Hounsell
type: post
date: 2023-11-06T00:00:00+00:00
url: /branding-apis/
featured_image: 3683-getosproductname.jpg
categories:
- APIs

---

After the painful experience of repeatedly changing names during the Windows XP and Windows Server 2003 development cycles, Microsoft decided to introduce a common, global branding API into Longhorn. The foundations of this system are present right from build 3683.

A new API was introduced in the kernel, called `RtlGetOSProductName`, which was then exposed to user space through `GetOSProductName` in `kernel32.dll`.

Dialogs like `sysdm.cpl` use this API to obtain branding information like the SKU and version. The argument of the function is a 7-bit long binary value. This value functions as a bit-mask, enabling/disabling certain parts of the branding information.

* **Company:** Microsoft
* **Family (Generic):** Windows
* **Family (Specific):** Longhorn XP
* **Type:** Professional
* **Version:** Version 2003
* **Service Pack:** _(Blank by default in all Longhorn builds)_
* **Copyright:** Copyright © 1981-2003 Microsoft Corporation

The binary value 1111111 (127 in decimal, or 0x7F in hexadecimal) will yield the complete branding string of all the above info concatenated.

The application below uses P/Invoke to call the new function with decimals as argument. For demonstration purposes, the service pack version has been set to 1 in the registry for the screenshot below.


{{< figure src="3683-getosproductname.png" link="3683-getosproductname.png" target="_blank" caption="An application demostrating the different branding parts returned by the API" alt="An application demostrating the different branding parts returned by the API" >}}

The source code for this application is available on [GitLab](https://gitlab.com/experience-longhorn/version-api-demo).

The default screensaver in Longhorn build 3683, `logon.scr`, has been altered to take advantage of the branding API and uses one of its functions to load the appropriate branding bitmap.

In later builds, Microsoft would introduce [pig latin](/pig-latin/) into the branding API so that parts of the interface not using the Branding API could be tracked down and updated.