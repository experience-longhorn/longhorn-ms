---
title: Theme Format and Aero
author: Lucas Brooks
type: post
date: 2021-10-08T00:00:00+00:00
url: /theme-format-aero/
featured_image: private-theme-error.png
categories:
  - DWM & Graphics

---

_Thanks to Lucas Brooks for letting me use his post from [BetaArchive](https://www.betaarchive.com/forum/viewtopic.php?f=62&t=42640)._

I did a bit of research on Longhorn's theme format, and figured out some things perhaps never fully documented before. I wrote a .msstyles decompiler, that can turn compiled packthem v4 themes into .INIs and resources (like Windows XP themes).

## Theme Format

### Overview
The theme format (.msstyles) changed fundamentally compared to Windows XP, even with the earliest leaked build of Microsoft Longhorn. The underlying principles of the theme format, however remained the same. A theme can have multiple sizes and color schemes defined, and the theme engine will load your desired size/color scheme.

The biggest difference between the two is Microsoft Longhorn themes are mostly binary, whereas Windows XP themes are largely plaintext. I guess Microsoft moved to a binary format for performance improvements and to prevent unauthorized modifications/decompilation.

In Windows XP, the information about a theme (name, author, date, blah blah blah), sizes and color schemes are defined in themes.ini. In Longhorn, they are mostly in a VARIANT called RMAP. VARIANT is the binary representation of INI. The size and color scheme names are in a plain binary image named VMAP (Variant Map). The actual theme files are stored as VARIANTs and they are named as SIZE + COLOR SCHEME.

VARIANT files are headerless, and can be broken into individual sections. Each section is equivalent to a line in an INI file, and contains name, type, class ID, part ID, and state ID. The class ID will be used by the theme engine to work out which class that section belongs to, and class names are stored in a binary image named CMAP (Class Map). The part ID will be compared against the values defined in STBL to work out the part (like checkbox, background) that section belongs to. Finally, the state ID will again be used to work out the state (like normal, hovered or pressed) each section represents. Class ID will always be interpreted as something, and the rest can be zero. It looks a bit like this:

{{< highlight ini >}}
[class.part(state)]
name = value
{{< / highlight >}}

There are a few basic types defined in STBL (like int, bool, font, etc.), each with a different size (some are variable). Finally, the theme resources such as bitmaps and streams are referred by the VARIANTs as IDs, instead of names like in Windows XP. I believe the theme compiler automatically assigns resource IDs upon the compilation of a theme file.

### Theme Decompilation / Porting to Windows XP
Most Longhorn themes were ported to Windows XP by replacing Luna's resources with ones from Longhorn themes. Since I now have a working theme decompiler, a better way to port those themes is to decompile them back to INIs and recompile with the leaked Windows XP packthem app. This way you can get perfect results without the need of Longhorn's uxtheme.dll. Of course, the Windows UI changed a lot, and later themes like Slate, Jade or Aero will have to be adapted to work on Windows XP. Early versions of Plex can be recompiled easily, and works perfectly on Windows XP. [Download](plex-for-xp.zip)

{{< gallery 25 "plex-xp.png" >}}

Jade does not look as good as 3683's Plex:

{{< gallery 25 "jade-xp-1.png" "jade-xp-2.png" >}}

Post-reset's Aero:

{{< gallery 25 "post-reset-aero-xp.png" >}}

## Interesting Things

### UseComposition
Noticed a bool called UseComposition under \[globals\] in Jade, and is set to FALSE. Change it to TRUE and DWM will be automatically turned on when you apply the theme. It will also automatically enable DWM upon boot. For that PDC demo build, pretty sure this is how DWM was automatically enabled upon logging in.

### DemoCheck
When I decompiled the code responsible for Preview Pane Aurora, I found that it actually performs 2 checks to make sure people outside of Microsoft cannot enable it. The first one is the theme name, which must be Aero. The second one is very interesting, it passes as long as you are not using the actual private Lab06 Aero theme - the case for everyone basically, and this is why we can actually enable the Aurora.

The second check is performed by a function in uxtheme.dll called ApplyTheme. That function is also known as ThemepDemoCheck internally. It checks for the existence and accessibility of a file defined by the DemoCheck property under \[documentation\]. That file is normally located on Microsoft's network share, so nobody outside of Microsoft can access it. This means if the Aero theme .msstyles somehow leaks to the public, it will not give you APPA - in fact, it won't even be treated as a valid theme so you cannot even apply it.

As mentioned earlier, the only theme with that DemoCheck string is the private Aero theme, and this is why you can enable APPA using an edited Jade theme. ThemepDemoCheck is performed every time a theme selected, and the theme will not apply if it fails. Microsoft of course didn't want public themes like Slate, Jade and Luna to not work, so they made the check fail only if the theme contains the DemoCheck documentation property, and it as a path name isn't accessible/does not exist. This means it will pass for all public themes, and APPA will render as long as there is a valid BAML defined under \[PreviewPane.Aurora\].

If you somehow have a copy of the private Aero theme, you will see the following error upon selecting it:

{{< figure src="private-theme-error.png" title="If you had a copy of the private Aero theme, you would see this error upon selecting it" >}}

Microsoft wouldn't be Microsoft if they didn't leave a backdoor. To bypass it, you can simply create the REG_DWORD `HKLM\Software\Microsoft\Windows\CurrentVersion\ThemeManager\DemoCheckOverride` and set it to `64222`, or `0xFADE` in hexadecimal.

### Desktop Aurora
For M7 builds, in STBL01 you can see the following three lines:

{{< highlight ini >}}
1,DESKTOPBACKGROUND
1,DESKTOPBACKGROUNDPARTS,,ENUMDEF,ENUMDEF
1,AURORA,DESKTOPBACKGROUNDPARTS,ENUMVAL,1,0
{{< / highlight >}}

This means it is possible for a theme to have a section heading named \[DesktopBackground.Aurora\]. If the BAMLSTREAM under \[PreviewPane.Aurora\] gives Preview Pane Aurora, it is logical to conclude that Desktop Aurora can also be enabled. What comes under that heading is unknown, but there is the possibility that Desktop Aurora was never fully implemented, as it wasn't showcased in any demo builds.

### Aero Theme
I don't believe anyone patched Jade to utilize UseComposition yet, so I made an Aero theme (for 4074) that enables DWM upon boot, and is capable of drawing Glass frames without the need of desksrv.dll resources. Of course, to make it as close to the actual private Aero theme as possible, I've also added DemoCheck to \[documentation\], which means it will throw that access denied error. Make sure you add the registry entry before applying it. Download [here](/download/aero-4074.zip).