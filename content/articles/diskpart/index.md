---
title: Disk partitioning
author: Melcher
type: post
date: 2014-10-25T16:33:28+00:00
url: /diskpart/
featured_image: longhorn-setup.png
categories:
  - Installer & Setup

---

In many Longhorn builds, there are issues with partitioning and formatting disks within the installer. Microsoft DiskPart is a utility included within the Windows Pre-installation Environment that allows you to partition disks from a command line interface.

Within the Windows Installer, press `Shift + F10`. This will bring up a command prompt window. In this window, type `diskpart` to open the tool and begin re-partioning and formatting.

To partition a disk execute the following commands line by line.

{{< highlight batchfile >}}
select disk 0
clean
create partition primary
select partition 1
active
assign
exit
{{< / highlight >}}

These steps work as follows:

1. Select the first disk (disk numbers in Windows are zero-indexed).
2. Erase any existing partitions on the disk and initialise an empty master boot record (MBR).
3. Create a new primary partition. Since no size is specified (via `size={megabytes}`), it will use the entire available space on the disk.
4. Select the first partition on the disk (partition numbers in Windows are one-indexed, unlike disks).
5. Mark the selected partition as active. With MBR disks, this marks the bootable partition.
6. Assign the partition a drive letter.
7. Exits the disk partitioning utility.

You now have created a partition on disk 0 which is assigned a drive letter and is active. If there are multiple drives in the system, be sure to select the correct disk. To get a list of disks, you can type `list disk`.

Most commands within DiskPart have three-letter variations that are supported.

{{< grid >}}
{{< figure src="diskpart-session.png" link="diskpart-session.png" target="_blank" alt="Re-partitioning a disk using diskpart in Windows PE" >}}
{{< figure src="lazy-diskpart-session.png" link="lazy-diskpart-session.png" target="_blank" alt="Re-partitioning a disk using diskpart in Windows PE, with short commands" >}}
{{< /grid >}}

## Formatting

Sometimes it's not enough to just partition the drive. A couple of Longhorn installers also have trouble formatting the drive. You can manually format a drive by executing:

{{< highlight batchfile >}}
format fs=ntfs quick
{{< / highlight >}}

In the example above the currently selected volume gets formatted with the NTFS file system. "Quick" formatting will not erase the entire drive, like normal formatting would, but simply overwrite the relevant file system structures.

## Executing setup from WinPE

If the Longhorn image doesn't boot at all, it's also possible to complete above steps and start the Longhorn setup from a WinPE that's on another image. With other Longhorn and earlier pre-reset WinPEs, this will require a second CD or DVD drive as WinPE requires the presence of the original media. For Vista RTM and onwards, you can remove the disk containing the original WinPE and insert the Longhorn disk, because the WinPE is loaded entirely into memory until the computer is restarted.

Boot from any Windows installation disc that uses WinPE (In the screenshots, Windows 7 has been used. Insert the Longhorn disc into the second drive, if you are using two CDs. Once the WinPE is loaded, simply press Shift + F10 and diskpart away!

At this point, if you are using only the one drive,you can insert the Longhorn disk. To start the Longhorn setup navigate to the correct drive in command prompt (`X:`, likely it's drive `D:` if you're using one drives, or drive `E:` if using two) and execute `setup`. The Longhorn setup will commence!

{{< figure src="longhorn-setup.png" title="Longhorn setup started from Windows 7 WinPE" >}}