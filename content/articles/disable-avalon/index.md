---
title: 'Hacking Avalon #1: Disable it in Longhorn'
author: Melcher
type: post
date: 2016-03-14T22:01:47+00:00
url: /disable-avalon/
featured_image: hackingavalon.png
categories:
  - APIs

---
This is the first post in a series that I'll be doing. Hacking Avalon will be all about interesting stuff in Avalon. Furthermore, I hope to provide some background on how early variants of Avalon work together with the shell in Longhorn. Keep in mind, this series will mainly discuss the earliest revisions of Avalon found in Milestone 3 builds. The tricks may not always work on later builds. That's it for the intro.

Now onto the subject of this post; disabling Avalon in explorer. The addition of Avalon to explorer makes for some interesting new functionality, but doesn't exactly speed up browsing. There are a couple of ways to gracefully disable Avalon in explorer without breaking other Avalon features.

## Option 1
This option removes the task pane from explorer altogether. You will end up with just the list view.

1. Open System Properties (sysdm.cpl).
2. Open the Advanced tab and Setting in the Performance settings.
3. On the Visual Effects tab deselect ‘Use common tasks in folders’ and Apply.

## Option 2
This alternative option will restore the version of the task pane used within Windows XP.

1. Open the Registry Editor (regedit.exe)
2. Browse to `HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\WebView`
3. Remove the GUID from the (Default) entry.

This GUID tells explorer which COM class to load and use for the Shell View provider. Without this information explorer falls back to the older XP view. Note that while Avalon is disabled explorer will still happily display WinFS pivots in the task pane.

{{< grid >}}
{{< figure src="explorer_w_avalon.png" link="explorer_w_avalon.png" target="_blank" title="With Avalon" alt="With Avalon" >}}
{{< figure src="explorer_wo_avalon.png" link="explorer_wo_avalon.png" target="_blank" title="Without Avalon" alt="Without Avalon" >}}
{{</ grid >}}