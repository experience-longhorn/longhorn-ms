---
title: protoPlex
author: Thomas Hounsell
type: post
date: 2021-05-15T00:00:00+00:00
url: /protoplex/
categories:
- Projects

---

"protoPlex" was a project that ran during 2008, organised by Thomas Hounsell and overlapping heavily with the Longhorn 08 team. The aim was to build on a Longhorn build 3718 base, and develop some early plex concepts demonstrated by Microsoft.

Backups of the downloads below, and other files taken from the protoPlex project can be found on the [Internet Archive](https://archive.org/details/protoplex).

## February 2008 CTP
This was the only public release from the protoPlex project. It included a somewhat complete version of the "bluePlex" theme. This is the only release that was delivered as an installer. It was later circulated as part of an ISO with some other collected patches for build 3718.

[Download protoPlex Feb 2008 CTP](BluePlex-Feb2008CTP.zip)

{{< gallery 25 "feb-itp-1.jpg" "feb-itp-2.jpg" "feb-itp-3.jpg" "feb-itp-4.jpg" >}}

## greenPlex Internal Preview
This was an internal release of the "greenPlex" theme, distributed around the time of the February CTP.

[Download greenPlex Internal Preview](greenPlex-Internal-Preview.zip)

{{< gallery 25 "greenplex-1.jpg" "greenplex-2.jpg" >}}

## Beta 1 release
This was due to be released publicly in April 2008, but for varying reasons, it never happened. Three themes are included - revised versions of the already-demonstrated "bluePlex" and "greenPlex" themes, plus an alternate "alphaPlex" theme, which was a lighter, softer blue than the darker, glossier "bluePlex" theme.

[Download protoPlex Beta 1](protoPlexAprBeta1.rar)

{{< gallery 25 "beta-1.png" "beta-2.png" "beta-3.png" "beta-4.png" >}}
{{< gallery 25 "beta-5.png" "beta-6.png" "beta-7.png" "beta-8.jpg" >}}